We love contributions! All we ask is that you submit an issue and, if a correction
to the repository is called for by the issue, then please feel free to submit a
subsequent PR to address the correction. But just submitting the issue without any
PR is a contribution! Someone will promptly review the issue and triage it
accordingly.